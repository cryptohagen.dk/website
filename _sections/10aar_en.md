## #cryptohagen – Ten years of privacy protection help

After Edward Snowden’s revelations in 2014, a group of activists decided that privacy
protection help was needed. Not as grandiose top-down solutions for user privacy and cyber
security but rather within an open-source tradition with constructive bottom-up solutions –
and with time for a quiet conversation about individual needs. Afterwards you can implement
possible actual solutions based on the motto that “your threat image is not necessarily my
threat image.” This conversation often begins with the individual’s need for privacy and
security, but in most cases, it also includes the need to protect the others one communicates
with.

## Once a month
Rather than using a lot of resources to create a bureaucratic structure, Cryptohagen’s
solution was very simple: They met at a café on Sunday afternoon once a month, where those
who needed help could drop by. Cryptohagen was created by busy IT people who decided to
help others. A monthly meeting was realistically resourced. But it could not be done without a
foothold in the open source environment, e.g. [Bornhack][bornhack], [data.coop][data.coop], [Labitat][labitat] og [IT-Politisk Forening][itpolitiskforening].

A core principle in Cryptohagen is <i>continuity, continuity, continuity</i>, since the individual user
who needs help rarely has the resources to reach out to find this help and is often in a very
stressful situation. This is why Cryptohagen still meets up on the last Sunday of the month at
Café Mellemrummet at Nørrebro in Copenhagen (except in December).

## Also in Aarhus
In 2019, a group of activists in Aarhus decided to make use of the experiences from
<b>#cryptohagen</b> and establish <b>#cryptoaarhus</b> – with meetings on the second Saturday of every
month. Since Cryptohagen’s Copenhagen website is naturally open source, it was easy for <b>#cryptoaarhus</b> to clone it. Gradually, in <b>#cryptohagen</b>, in addition to helping those who
passed by in need of help, a need arose for us as a <b><i>“friendly community”</i></b> to update each other
with presentations and deal with the rapidly increasing attack on the right to privacy (online
and offline) and especially the global threats to the right to encryption.
And, unfortunately, that work probably won’t end any time soon.

## Nomination for the Libre Prize
In the summer of 2024, <b>#cryptohagen</b> and <b>#cryptoaarhus</b> were nominated for the [Libre Prize][libraprisen] for the following reason:

<i>“Cryptohagen is nominated for the Libre Prize 2024 because of a long and commendable effort to
inform ordinary citizens at a grassroots level about their option to secure their own privacy on their
own computers. The form is informal: Everyone can show up with their computer under their arm at a
cafe in Copenhagen or Aarhus at a monthly meeting, where various activists then make their expertise
available to those who want to learn more about privacy protection and cyber security. People who
have been subjected to stalking or other digital abuse are encouraged to come forward and get help.
With their form and voluntarism, Cryptohagen shows civic-mindedness and genuine concern for their
fellow human beings. In a digital age where our basic civil rights are under pressure from tech giants,
opaque algorithms and artificial intelligence, Cryptohagen sets a direction that is much needed. This
is why Cryptohagen is nominated for the Libre Prize 2024.”</i>

It is <b>#cryptohagen’s</b> wish that other groups, like the activists in <b>#cryptoaarhus</b>, in the best
open-source tradition establish themselves in several places throughout the country.
Thank you for the first ten years.

[cryptohagen]:https://cryptohagen.dk
[cryptoaarhus]:https://cryptoaarhus.dk
[bornhack]:https://bornhack.dk
[data.coop]:https://data.coop
[labitat]: https://labitat.dk
[itpolitiskforening]: https://itpol.dk
[libraprisen]: https://libre-prisen.dk