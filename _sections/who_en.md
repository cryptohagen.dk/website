# About us
Cryptohagen is a cozy and informal community with a focus on digital privacy. Cryptohagen
is for anyone who uses the Internet in one form or another. We are a ”friendly community”
where you can share thoughts and ideas about privacy, online safety, encryption, and
similar topics. Everyone is welcome.

We can help you with many kinds of questions about digital privacy. We can also help you
get started using some of the popular and recognized security tools on your smartphone
and on your computer. We can talk about that when we meet. Your specific needs are our
starting point.

We get together every last Sunday of the month in Copenhagen at Café Mellemrummet. 
The café is a volunteer-run non-profit café. We expect you to be open and empathetic to the volunteers in the café as well. There is a similar event in
[Aarhus][cryptoaarhus]. 

So come and say hi, have a chat and a good cup of coffee. Ask for Thomas, Rolf,
Ove, or Helle. We’ll have a talk about your specific needs and come up with some ideas to
help you get started. All you need to bring is a friendly smile and a wish to get to know some
nice people in a relaxed environment.

Now and then we invite a speaker, or we update each other on new knowledge. We also
organize an annual event on Tor Project and other anonymous browsers (based on State of
the Onion).

Cryptohagen interacts as a community with [Bornhack][bornhack], [data.coop][data.coop], [Labitat][labitat] and [IT-Politisk Forening][itpolitiskforening].


[#Cryptohagen][cryptohagen] and [#Cryptoaarhus][cryptoaarhus] were among the nominees for the Danish [Libre-prisen 2024 ][libraprisen].

Read our [Code of Conduct][bornhackconduct].

# Who are you?
Sometimes you need to surf the Internet or communicate with others without anybody
”listening in”. This also applies to you who think you have nothing to hide.

Maybe you belong at one of these levels:

- You are a student and need to research sensitive topics.
- You are in a problematic relationship and need a way to get help without alerting
your partner.
- You may be a future whistleblower.
- You have been hacked.

We can help you at all the above-mentioned levels. We are not against digitization, but we
do appreciate the rights of privacy – also on the Internet.

# Get in touch
We have a [Mastodon account][mastodon] / [Get on mastodon][joinmastodon]
The best way to get in touch: Come and meet us every last Sunday of the month in
Copenhagen!

[cryptohagen]:https://cryptohagen.dk
[cryptoaarhus]:https://cryptoaarhus.dk
[bornhackconduct]:https://bornhack.dk/conduct
[adfærdskodeks]:https://cryptohagen.dk/code.html
[#where]:https://cryptohagen.dk/#where
[twitter]:https://twitter.com/cryptohagen
[mastodon]:https://social.data.coop/@cryptohagen
[joinmastodon]: https://joinmastodon.org/
[bornhack]:https://bornhack.dk
[data.coop]:https://data.coop
[labitat]: https://labitat.dk
[itpolitiskforening]: https://itpol.dk
[libraprisen]: https://libre-prisen.dk

