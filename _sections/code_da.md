# Adfærdskodeks
<i>(Code of Conduct)</i>

Vi er forpligtede til at gøre #cryptohagen til et inkluderende og indbydende forum for alle. 
Den vigtigste regel er: Vær åben og empatisk overfor hinanden.

Desværre er teknologibegivenheder berygtede for chikane. Vi har derfor lavet nogle grundregler – for at gøre det klart, at chikane eller diskriminerende adfærd ikke er acceptabelt. Alle skal kunne føle sig trygge ved at deltage i #cryptohagen.

Alle, der deltager i #cryptohagen, er forpligtede til at overholde vores adfærdskodeks (Code of Conduct) ved arrangementer og på alle onlinekanaler. 

Specifikke regler:
- Vi tager afstand fra homofobisk, transfobisk, sexistisk, racistisk og anden fordomsfuld adfærd (herunder chikane). 
- #cryptohagen er et fælles forum, men hver deltagers personlige ”rum” skal respekteres. Hvis du bliver bedt om at lade nogen være i fred, eller om at forlade stedet, må du respektere dette.
- Nogle deltagere ønsker ikke at blive filmet eller fotograferet. Respekter deres ønsker.
- Aggressive eller arrogante kommentarer er ikke velkomne – ingen skal være bange for at stille spørgsmål.

Hvis du bryder disse regler, kan vi udelukke dig fra fremtidige #cryptohagen-begivenheder.

Café Mellemrummet, hvor vi mødes i København, er en frivilligt drevet nonprofitcafé. Vi forventer, at du også er åben og empatisk overfor de frivillige i caféen.

Hvis du bliver chikaneret, eller du er vidne til brud på vores adfærdskodeks, skal du kontakte en fra kernegruppen.

Vores adfærdskodeks er baseret på (”lånt” fra) vores venner på Bornhack og deres [adfærdskodeks][bornhackconduct]

[bornhackconduct]:https://bornhack.dk/conduct
