# Code of Conduct
We are committed to making #cryptohagen an inclusive and welcoming get-together for everyone.
The most important rule is: Be open and empathetic towards each other.

Unfortunately, tech events are notorious for harassment. Thus, we have set up some basic rules to make it clear that harassment or discriminatory behavior will not be tolerated. Everyone should feel safe participating in #cryptohagen.

Everyone participating in #cryptohagen is obliged to comply with our Code of Conduct at events as well as on all online channels. 

Specific rules:
- We denounce homophobic, transphobic, sexist, racist and other prejudiced behaviour (including harassment). 
- #cryptohagen is a community, but each participant’s personal ”space” should be respected. If you are asked to leave someone alone, or to leave the place, then respect this. 
- Some participants do not want to be filmed or photographed. Respect their wishes. 
- Aggressive or arrogant comments are not welcome – no one should be afraid to ask questions.

If you break these rules, we may exclude you from future #cryptohagen events.

Café Mellemrummet, where we meet in Copenhagen, is a volunteer-run nonprofit café. We expect that you are open and empathetic towards the volunteers in the café as well. If you are being harassed, or if you witness a breach of our Code of Conduct, please contact someone from the core group.

Our Code of Conduct is based on (”borrowed” from) our friends at Bornhack and their [Code of Conduct][bornhackconduct]

[bornhackconduct]:https://bornhack.dk/conduct