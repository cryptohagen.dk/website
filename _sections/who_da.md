# Hvad er det?
Cryptohagen er et hyggeligt og uformelt forum, som har fokus på digital
privatlivsbeskyttelse. Cryptohagen er for alle, der bruger nettet i en eller anden form.
Vi er et ”friendly community”, hvor der er plads til at vende tanker og idéer om
privatliv, sikkerhed på nettet, kryptering og lignende emner. Alle er velkomne.

Vi kan hjælpe dig med små og store spørgsmål, som handler om
privatlivs-beskyttelse på nettet. Vi kan også hjælpe dig i gang med at bruge nogle af
de populære og anerkendte sikkerhedsværktøjer på din smartphone og på din
computer. Alt det kan vi tale sammen om, når vi ses. Så kan vi tage udgangspunkt i
dine konkrete behov.

Vi mødes i på Café Mellemrummet i København den sidste søndag i måneden. 
Caféen er en frivilligt drevet nonprofitcafé. Vi forventer, at du også er åben og empatisk overfor de frivillige i caféen.
Der afholdes lignende arrangementer i [Aarhus][cryptoaarhus]. 

Så kom og sig hej til os, drik en kop kaffe, og få en sludder. Spørg efter Thomas, Rolf, Ove eller Helle. Det eneste, du skal have med, er godt humør og ønsket om at lære nogle rare mennesker at kende i et afslappet miljø.

Af og til inviterer vi en oplægsholder, eller vi opdaterer hinanden om ny viden. Vi
laver også en årlig event om Tor Project og andre anonyme browsere (baseret på
State of the Onion).

Cryptohagen interagerer som et community med [Bornhack][bornhack], [data.coop][data.coop], [Labitat][labitat] og [IT-Politisk Forening][itpolitiskforening].

[#Cryptohagen][cryptohagen] og [#Cryptoaarhus][cryptoaarhus] var blandt de nominerede til den danske [Libre-prisen 2024][libraprisen].

Læs vores [adfærdskodeks][adfærdskodeks].

# Hvem er du?
Der vil være tidspunkter, hvor du har brug for at kunne bevæge dig rundt på nettet
eller kommunikere med andre, uden at nogen ”lytter med”. Det gælder også dig, som
ikke mener, du har noget at skjule.

Måske hører du til på et af disse niveauer:
- Du er studerende og skal søge oplysninger eller lave research om følsomme emner.
- Du er i et problematisk parforhold og vil kunne søge hjælp og oplysninger, uden at din partner bliver opmærksom på det.
- Du er en kommende whistleblower.
- Du er blevet hacket.

Vi kan hjælpe dig på alle niveauer. Vi er ikke modstandere af digitaliseringen, men
værdsætter retten til privatliv – også på nettet.

## Kom i kontakt
Vi er på [Mastodonkonto][mastodon] / [Kom på mastodon][joinmastodon]

Den bedste måde, du kan kontakte os, er at komme og hilse på os den sidste
søndag i måneden.

[cryptohagen]:https://cryptohagen.dk
[cryptoaarhus]:https://cryptoaarhus.dk
[adfærdskodeks]:https://cryptohagen.dk/code.html
[#where]:https://cryptohagen.dk/#where
[twitter]:https://twitter.com/cryptohagen
[mastodon]:https://social.data.coop/@cryptohagen
[joinmastodon]:https://joinmastodon.org
[bornhack]:https://bornhack.dk
[data.coop]:https://data.coop
[labitat]: https://labitat.dk
[itpolitiskforening]: https://itpol.dk
[libraprisen]: https://libre-prisen.dk


