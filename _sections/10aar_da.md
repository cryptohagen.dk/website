## #cryptohagen - Ti år med hjælp til privatlivsbeskyttelse

Efter Edward Snowdens afsløringer i 2014 besluttede en flok aktivister, at der var brug for hjælp til privatlivsbeskyttelse. Ikke i form af de grandiose oppefra og ned-løsninger til brugernes privatliv og cybersikkerhed, men i en open source-tradition med konstruktive løsninger nedefra og op – med tid til en stille og rolig snak om den enkeltes behov. Derfra kan man tage fat på mulige konkrete løsninger ud fra devisen om, at ”dit trusselsbillede ikke nødvendigvis er mit trusselsbillede.” Denne snak begynder ofte med, at den enkelte har behov for privatliv og sikkerhed, men de fleste gange kommer den også til at handle om den enkeltes behov for at kunne beskytte andre, som der kommunikeres med.

![Fra Cryptohagen](/assets/logo/10aar_img01.jpeg)

## En gang om måneden
I stedet for at bruge en masse ressourcer på at konstruere en bureaukratisk struktur, blev Cryptohagens løsning meget enkel: Man mødtes på en café en søndag eftermiddag en gang om måneden, hvor de, som havde brug for hjælp, kunne dukke op. Cryptohagen blev skabt af travle IT-folk, som besluttede at hjælpe andre. Et møde en gang om måneden var, hvad der realistisk var ressourcer til. Men det kunne ikke lade sig gøre uden en forankring i open source-miljøet, det vil sige med tilknytning til bl.a. [Bornhack][bornhack], [data.coop][data.coop], [Labitat][labitat] og [IT-Politisk Forening][itpolitiskforening].

Et kerneprincip i Cryptohagen er <i>kontinuitet, kontinuitet, kontinuitet</i>, da den enkelte bruger, som har behov for hjælp, sjældent har ressourcer til at lave opsøgende arbejde for at finde denne hjælp i en ofte meget stresset situation. Derfor mødes Cryptohagen stadig den sidste søndag i måneden på Café Mellemrummet på Nørrebro i København (undtagen i december).

## Også i Aarhus
I 2019 besluttede en gruppe aktivister i Aarhus at bruge erfaringerne fra <b>#cryptohagen</b> og etablere <b>#cryptoaarhus</b>, som mødes den anden lørdag i hver måned. Da Cryptohagens københavnske hjemmeside selvfølgelig er open source, var det nemt for <b>#cryptoaarhus</b> at klone den. 

Efterhånden opstod der i <b>#cryptohagen</b>, udover at hjælpe dem, som lagde vejen forbi med behov for hjælp, et behov for, at vi som <b><i>”friendly community”</i></b> opdaterede hinanden med oplæg og forholdt os til det stærkt stigende angreb på retten til privatliv (online og offline) og især de globale trusler mod retten til kryptering.
Og det arbejde slutter desværre nok ikke foreløbig.

## Nominering til Libre-Prisen
I sommeren 2024 blev <b>#cryptohagen</b> og <b>#cryptoaarhus</b> nomineret til [Libre-prisen 2024][libraprisen] med begrundelsen:

<i>”Cryptohagen nomineres til Libre-Prisen 2024 på baggrund af en lang og prisværdig indsats for på græsrodsniveau at oplyse almindelige borgere om deres mulighed for at sikre deres eget privatliv på egne computere. Formen er uformel: Alle kan møde op med deres computer under armen på en café i København eller Aarhus på et månedligt møde, hvor forskellige aktivister så stiller deres ekspertise til rådighed for dem, der gerne vil lære mere om privatlivsbeskyttelse og cybersikkerhed. Mennesker, der har været udsat for stalking eller andet digitalt misbrug opfordres til at møde op og få hjælp. Med deres form og frivillighed viser Cryptohagen borgersind og ægte omsorg for deres medmennesker. I en digital tidsalder, hvor vores basale borgerrettigheder er under pres fra tech giganter, uigennemsigtige algoritmer og kunstig  intelligens, sætter Cryptohagen en retning, der i høj grad er nødvendig. Derfor nomineres Cryptohagen til Libre-Prisen 2024.”</i>

![Libraprisen - nominering](/assets/logo/10aar_img03.jpeg)

Det er <b>#Cryptohagen</b> ønske, at andre grupper, ligesom aktivisterne i <b>#Cryptoaarhus</b>, i bedste open source-tradition etablerer sig flere steder i landet.
Tak for de første ti år.




[cryptohagen]:https://cryptohagen.dk
[cryptoaarhus]:https://cryptoaarhus.dk
[bornhack]:https://bornhack.dk
[data.coop]:https://data.coop
[labitat]: https://labitat.dk
[itpolitiskforening]: https://itpol.dk
[libraprisen]: https://libre-prisen.dk
